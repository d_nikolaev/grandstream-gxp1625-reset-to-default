# Сброс настроек телефона Grandstream GXP1625 до заводских
## Скрипт для Centos 7 на Python3.6. Предназначен для сброса настроек на Grandstream GXP1625 до заводских

### Устанавливаем python 3 версии   
`yum update -y`   
`yum install epel-release -y`  
`yum install python3 -y`  

### Устанавливаем git и настраиваем его   
`yum install git -y`   
`git config --global user.name "Ваше имя"`   
`git config --global user.email "youremail@yourdomain.com"`  

### Переходим в директорию, где будет располагаться бот (например в /root)   
`cd /root`   

### Клонируем проект в директорию /root и переходим только что созданную папку   
`git clone https://gitlab.com/d_nikolaev/grandstream-gxp1625-reset-to-default`   
Вводим логин и пароль   
`cd ./grandstream-gxp1625-reset-to-default`   

### Открываем файл main.py на редактирование, указываем админский пароль для авторизации на телефоне
`pwd = '<PASSWORD>'`  

### Создаём здесь виртуальное окружение
`python3 -m venv venv`   

### Активируем виртуальное окружение
`source venv/bin/activate`   

### Выполняем апгрейд pip
`pip install --upgrade pip`   

### Ставим библиотеки из файла requirements.txt
`pip install -r requirements.txt`   

### Делаем скрипт исполняемым   
`chmod ugo+x main.py`    

### Запускаем скрипт
`./main.py`   

### По окончании деактивируем виртуальную среду
`deactivate`  
