import requests
import json
import ipaddress


def inputIP():
    """Запрос IP-адреса у пользователя"""
    while True:
        IP = input("Введите IP-адрес телефона: \n")
        if check_ip(IP):
            return IP
        else:
            print("Введен некорректный IP-адрес")


def check_ip(ip_address):
    """Проверка корректности ввода IP-адреса"""
    try:
        ipaddress.ip_network(ip_address)
        return True
    except:
        return False


def factory_reset_gs(ip: ipaddress.ip_address = "10.136.122.20", pwd: str = "admin"):
    """
    Сброс до заводских настроек телефона Grandstream GXP1620/1625
    """
    if not check_ip(ip):
        print("[ERROR] Ошибка в IP-адресе")
        return
    referer = f"http://{ip}/"
    with requests.Session() as s:
        ### Авторизация на телефоне, получение Session ID
        url = f"{referer}cgi-bin/dologin"
        login = 'admin'
        headers = {
            'Referer': referer,
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'
        }
        data = {'username': login, 'password': pwd}
        try:
            r = s.post(url, data=data, headers = headers, verify=False, timeout=5)
            if r.json().get("response") == "success":
                sid = r.json().get("body").get("sid")
            else:
                print(f"[ERROR] Не удалось авторизоваться - {r.json()}")
                return
        except:
            print("[ERROR] Не удалось авторизоваться. Это точно Grandstream GXP1620/1625?")
            return
        
        ### Сброс настроек телефона до заводских
        url = f"{referer}cgi-bin/api-sys_operation"
        data = {'request': 'RESET', 'sid': sid}
        try:
            r = s.post(url, data = data, headers = headers, verify=False, timeout=3)
            print("Result: ", r.json().get("response")) # Вывод результата: error или success
        except:
            print("[ERROR] Не удалось сбросить настройки")
            return


def main():
    ip = inputIP() #Запрос IP-адреса у пользователя
    pwd = '<PASSWORD>' # Пароль для авторизации
    factory_reset_gs(ip=ip, pwd=pwd)


if __name__ == '__main__':
    main()

